﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameObject player;
    /*
    public Animator space;
    public Animator tab;
    public Animator shift;
    */
    
    private int puntuacion;
    private int level = 1;

    private bool eliminado = false;

    private Marcador marcador;

    public bool getEliminado()
    {
        return this.eliminado;
    }
    public void setEliminado(bool variable)
    {
        this.eliminado = variable;
    }

    public int Puntuacion
    {
        get
        {
            return puntuacion;
        }

        set
        {
            if (value < 0)
            {
                value = 0;
            }
            puntuacion = value;
            MostrarPuntuacion();
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        marcador = FindObjectOfType<Marcador>();
        MostrarPuntuacion();
        //mensajeFinal.SetActive(false);
        //space.SetBool("entrarEspacio", true);
    }

    // Update is called once per frame
    bool boton1 = false;
    bool boton2 = false;
    bool boton3 = false;
    void Update()
    {
        /*
        if (Input.GetButton("Jump"))
        {
            space.SetBool("salirEspacio", true);
            shift.SetBool("mayusEntra", true);
            
        }

        if (Input.GetButton("Fire1"))
        {
            shift.SetBool("mayusSale", true);
            tab.SetBool("tabEntra", true);
           
        }

        if (Input.GetButton("Fire2"))
        {
            tab.SetBool("tabSale", true);
        }
        */
        /*
        string name = SceneManager.sceneLoaded;
        if(SceneManager.sceneLoaded)
        */
    }

    private void MostrarPuntuacion()
    {
        marcador.Actualizar(Puntuacion);
    }


    /*public void ComprobarVictoria()
    {
        if (FindObjectsOfType<PickUp>().Length == 1)
        {
            //Debug.Log("Victory!!!!!!!!!!!!!!!!!");
            mensajeFinal.SetActive(true);
        }
    }


    public void Reiniciar()
    {
        SceneManager.LoadScene("SampleScene");
    }*/
}

