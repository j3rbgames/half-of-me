﻿using UnityEngine;
using System.Collections;

public class Tiempo : MonoBehaviour
{
    public GameObject projectile;

    void Start()
    {
        InvokeRepeating("LaunchProjectile", 2.0f, 20.0f);
    }

    void LaunchProjectile()
    {
        GameObject instance = Instantiate(projectile);
        Rigidbody rb = instance.GetComponent<Rigidbody>();
        rb.velocity = Random.insideUnitSphere * 5;
    }
}
