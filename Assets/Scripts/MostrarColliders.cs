﻿// using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarColliders : MonoBehaviour
{
    void OnDrawGizmos()
    {
        BoxCollider bCol = GetComponent<BoxCollider>();
        // SphereCollider sCol = GetComponent<SphereCollider>();

        Gizmos.color = Color.cyan;

        if (bCol != null)
        {
            Gizmos.DrawWireCube(bCol.bounds.center, bCol.bounds.size);
        }

        /*
        if (sCol != null)
        {
            float maxScale = Math.Max(gameObject.transform.localScale.x,
            Math.Max(gameObject.transform.localScale.y, gameObject.transform.localScale.z));

            Gizmos.DrawWireSphere(sCol.bounds.center, sCol.radius * maxScale);
        }
        */
    }
}
