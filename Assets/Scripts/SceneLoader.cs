﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public void LoadScene(int level)
    {
        SceneManager.LoadScene(level);
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Estoy onTrigger");
        switch (other.gameObject.tag)
        {
            case "Player":
                
                SceneManager.LoadScene(1);
                break;
        }
    }
}