﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickup : MonoBehaviour
{
    //public Transform fx;

    public int puntos = 1;

    public Transform fx;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            Instantiate(fx, transform.position, Quaternion.identity);
            gameManager.Puntuacion += puntos;
            //gameManager.ComprobarVictoria();
        }
    }

}
