﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarItem : MonoBehaviour
{
    public Quaternion from = Quaternion.Euler(0f, 0f, 0f);
    public Quaternion to = Quaternion.Euler(0f, 180f, 0f);
    public float speed = 20f;
    private float yInicial, yPosition, increaseYPosition;

    private void Awake()
    {
        yInicial = transform.position.y;
        yPosition = yInicial;
        // Random speed up/down.
        increaseYPosition = (1.0f * Mathf.PI) / 10f;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = Quaternion.Lerp(transform.rotation, to, Time.time * speed);
        transform.Rotate(Vector3.up, speed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, yInicial + 1.0f * Mathf.Sin(yPosition), transform.position.z);
        // Update the up/down position.
        yPosition += increaseYPosition * Time.deltaTime;
        if (yPosition > Mathf.PI)
        {
            yPosition = yPosition - Mathf.PI;
        }
    }
}
