﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlBoton : MonoBehaviour
{

    public Animator anim;

    plataformaBoton plataformaBoton;

    public bool boton = false;

    public AudioSource buttonSound;

    // Start is called before the first frame update
    void Start()
    {
        //anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (boton == true)
        {
            GameObject miObjeto = GameObject.FindWithTag("plataformaBoton");
            plataformaBoton = miObjeto.GetComponent<plataformaBoton>();
            plataformaBoton.boton();
        }
    }

    /*private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.gameObject.tag == "Player")
        {
            boton = true;
            anim.SetBool("pulsado", true);
        }
    }*/

    void OnCollisionEnter(Collision collision) { 

        if(collision.gameObject.tag == "Player")
        {
            boton = true;
            anim.SetBool("pulsado", true);
            Debug.Log("Despues de la animacion");
            buttonSound.Play();
        }


    }



}
