﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnergia : MonoBehaviour
{
    public GameObject energia;

    public GameObject corazon;

    public Vector3[] puntos = new Vector3[10];

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn", 3.0f, 40.0f);
        InvokeRepeating("Life", 5.0f, 70.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {

        for (int i = 0; i < 3; i++)
        {
            int rnd = UnityEngine.Random.Range(0, puntos.Length);
            Instantiate(energia, puntos[rnd], Quaternion.identity);
        }
    }
    void Life()
    {
        int rnd = UnityEngine.Random.Range(0, puntos.Length);
        Instantiate(corazon, puntos[rnd], Quaternion.identity);
    }
}
