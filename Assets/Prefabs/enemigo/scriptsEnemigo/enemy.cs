﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemy : MonoBehaviour
{
    public float health;

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(health);

        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        other.CompareTag("onda");
        health = health - 5;
    }
}
