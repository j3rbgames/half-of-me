﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Patruller : MonoBehaviour
{
    public Transform player;

    //public float radio = 6f;
    public Vector3 rangoV = new Vector3(4, 3, 3);
    public float rango = 4;

    public Transform[] ruta;

    private int destino = 0;

    private NavMeshAgent agente;

    public enum EstadosPatrullero
    {
        Patrulla,
        Ataque
    }

    private EstadosPatrullero _estado = EstadosPatrullero.Patrulla;

    public EstadosPatrullero Estado {
        get {
            return _estado;
        }
        set {
            _estado = value;
            if(_estado == EstadosPatrullero.Patrulla)
            {
                siguientePunto();
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (Estado == EstadosPatrullero.Ataque) Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, rangoV);
        
    }

    // Start is called before the first frame update
    void Start()
    {
        agente = GetComponent<NavMeshAgent>();
        //agente.destination = punto.position;

        //siguientePunto();

        
    }

    float secondsCounter = 0;
    float secondsToCount = 2;

    // Update is called once per frame
    void Update()
    {
        switch (Estado)
        {
            case EstadosPatrullero.Patrulla:
                if (!agente.pathPending && agente.remainingDistance < .5f)
                {
                    secondsCounter += Time.deltaTime;
                    if (secondsCounter >= secondsToCount)
                    {
                        secondsCounter = 0;
                        siguientePunto();
                    }
                }
                //Para un rango cirsular if (Vector3.Distance(transform.position, player.position) < radio)
                if(((transform.position.x - player.position.x) < rango) && ((transform.position.y - player.position.y) < 0.2 ))
                {
                    Debug.Log("Distancia entre x: " + (transform.position.x-player.position.x));
                    Estado = EstadosPatrullero.Ataque;
                    agente.autoBraking = false; //desactiva el autofrenado para cuando va a atacar que no se frene al acercarse
                }
                break;

            case EstadosPatrullero.Ataque:
                agente.destination = player.position;
               //PAra un rango circular if (Vector3.Distance(transform.position, player.position) > radio)
               if(((transform.position.x - player.position.x) > rango) || ((transform.position.y - player.position.y) > 0.2))
                {
                    Debug.Log("Distancia entre x: " + (transform.position.x - player.position.x));
                    Estado = EstadosPatrullero.Patrulla;
                    agente.autoBraking = true; //activa el autofrenado para el modo patrulla
                }
                break;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Distancia en el eje x" + (transform.position.x - player.position.x));
            Debug.Log("Distancia en el eje y" + (transform.position.y - player.position.y));
        }
        

    }

    void siguientePunto()
    {
        if(ruta.Length == 0)
        {
            return;
        }
        else
        {
            agente.destination = ruta[destino].position;
            destino = (destino + 1) % ruta.Length; //aumentamos el valor del indice, obtengo el resto de dividirlo por la longitud del array
                                                    //cuando destino es igual que la longitud del array el calculo valdra 0 y volvera a empezar
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Escenario_inicial");

        }
    }*/
}
