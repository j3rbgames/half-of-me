﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemyF : MonoBehaviour
{
    public float health;

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(health);

        if(health == 0)
        {
            SceneManager.LoadScene("ScoreMenu");
        } 
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("onda")) { 
        health = health - 5;
        }

    }
}
