﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour
{
    public GameObject projectile;
    //public GameObject bomba;
    public Vector3[] puntos = new Vector3[10];
    public Renderer rend;
    public Color amarillo = new Color(195, 211, 104 , 255);
    public Color rojo = Color.red;


    void Start()
    {
        InvokeRepeating("LaunchProjectile", 3.0f, 20.0f);
        //InvokeRepeating("LaunchBomb", 5.0f, 35.0f);
        rend = GetComponent<Renderer>();
    }

    void LaunchProjectile()
    {
        
        StartCoroutine(Example());

        
    }

    IEnumerator Example()
    {
        rend.material.color = rojo;
        yield return new WaitForSeconds(0.6f);
        rend.material.color = amarillo;
        yield return new WaitForSeconds(0.3f);
        rend.material.color = rojo;
        yield return new WaitForSeconds(0.3f);
        rend.material.color = amarillo;
        yield return new WaitForSeconds(0.1f);
        rend.material.color = rojo;
        yield return new WaitForSeconds(0.1f);
        rend.material.color = amarillo;

        for (int i = 0; i < 7; i++)
        {
            int rnd = UnityEngine.Random.Range(1, 14);
            Instantiate(projectile, new Vector3(i * 5.0F, (float)rnd, 0), Quaternion.identity);
        }
    }

    /*void LaunchBomb()
    {
        int rnd1 = UnityEngine.Random.Range(0, puntos.Length);
        Instantiate(bomba, puntos[rnd1], Quaternion.identity);
        /*GameObject bmb = bomba;
        Vector3 vector3 = bomba.transform.position;
        DestroyImmediate(bmb, true);
        Instantiate(projectile, vector3, Quaternion.identity);
        
    }*/

}
