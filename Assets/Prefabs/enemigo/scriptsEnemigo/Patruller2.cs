﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Patruller2 : MonoBehaviour
{
    public Transform[] ruta;

    private int destino = 0;

    private NavMeshAgent agente2;

    // Start is called before the first frame update
    void Start()
    {
        agente2 = GetComponent<NavMeshAgent>();
        //agente.destination = punto.position;

        //siguientePunto();

        
    }

    float secondsCounter = 0;
    float secondsToCount = 2;

    // Update is called once per frame
    void Update()
    {
        if (!agente2.pathPending && agente2.remainingDistance < .5f)
        {
            secondsCounter += Time.deltaTime;
            if (secondsCounter >= secondsToCount)
            {
                secondsCounter = 0;
                siguientePunto();
            }
        }
        
    }

    void siguientePunto()
    {
        if(ruta.Length == 0)
        {
            return;
        }
        else
        {
            agente2.destination = ruta[destino].position;
            destino = (destino + 1) % ruta.Length; //aumentamos el valor del indice, obtengo el resto de dividirlo por la longitud del array
                                                    //cuando destino es igual que la longitud del array el calculo valdra 0 y volvera a empezar
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "jugadorJoan")
        {
            SceneManager.LoadScene("ScenesMovEnemigos");
        }
    }
}
