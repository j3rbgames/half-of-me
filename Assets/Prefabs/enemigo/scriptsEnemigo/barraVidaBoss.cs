﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barraVidaBoss : MonoBehaviour
{
    private float vida = 0;
    float y = 0;
    GameObject varGameObject;

    // Start is called before the first frame update
    void Start()
    {
        varGameObject = GameObject.FindWithTag("boss");
        vida = varGameObject.GetComponent<enemyF>().health;
        y = vida;
    }

    // Update is called once per frame
    void Update()
    {
        vida = varGameObject.GetComponent<enemyF>().health;
        float x = (vida*0.2f)/y ;
        transform.localScale = new Vector3(x, 0.1f, 1);
    }

}
