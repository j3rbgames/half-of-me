﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnergiaPlayer : MonoBehaviour
{
    public energia energia_canvas;
    public int energias = 3;
    GameObject varGameObject;
    // Start is called before the first frame update
    void Start()
    {
        energia_canvas = FindObjectOfType<energia>();
        varGameObject = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        energia_canvas.cambioEnergia(energias);
        if (Input.GetButtonDown("Fire3") && energias > 0 && varGameObject.GetComponent<CogerObjeto>().enabled == false)
        {
            energias--;
            energia_canvas.cambioEnergia(energias);
            varGameObject.GetComponent<atacar>().enabled = true;
        }
        else if (energias == 0)
        {
            varGameObject.GetComponent<atacar>().enabled = false;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("powerup") && energias <3) {
            energias++;
        }
    }

}
