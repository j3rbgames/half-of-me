﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContrlPelota : MonoBehaviour
{

    public float velocidad = 10;

    private Rigidbody rb;

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Pelota Creada");
        //Obtener referencias a determinados componentes
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

    }

    private void OnCollisionEnter(Collision collision)//Da informacion sobre la colision
    {
        float v = collision.relativeVelocity.magnitude;
        audioSource.volume = v / 10;
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()// no se garantiza que siempre se ejecuta cada x tiempo, el tiempo puede variar
    {// en unity se puede ajustar en Edit> ProjectSettings > Time
        /*
        Vector3 movimiento = new Vector3(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical"));

        transform.Translate(movimiento * (Time.deltaTime * velocidad)); //Hace referencia a la propiedad transform del objeto a la cual esta asignado este script
        // Ahora multiplicamos por deltaTime para que se mueva a 1m/s porque delstaTime hace que la velocidad se refiera a segundos
        // Con la variable velocidad multiplicamos el movimiento por 10m/s
        */

    }

    private void FixedUpdate()// para periodos de tiempo fijos y que las fisicas no den problemas
    {
        Vector3 fuerza = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        rb.AddForce(fuerza * velocidad);
    }
}
