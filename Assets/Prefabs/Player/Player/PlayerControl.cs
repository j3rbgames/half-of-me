﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 movement = Vector3.zero;
    private CharacterController controller;

    public AudioSource jumpSound;

    public enum EstadosOrientacion
    {
        Sur,
        Este
    }

    private EstadosOrientacion orientacion = EstadosOrientacion.Sur;

    public EstadosOrientacion Orientacion { get => orientacion; set => orientacion = value; }

    // Awake is called on script load
    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // restringir movimiento eje Z
        Vector3 position = transform.position;
        position.z = 0;
        transform.position = position;

        if (controller.isGrounded)
        {
            switch (Orientacion)
            {
                case EstadosOrientacion.Sur:
                    movement = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
                    break;
                case EstadosOrientacion.Este:
                    movement = new Vector3(0, 0, Input.GetAxis("Horizontal"));
                    break;
            }

            if (movement != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement.normalized), 0.2f);
            }
            //movement = transform.TransformDirection(movement);
            movement *= speed;
            if (Input.GetButton("Jump"))
            {
                movement.y = jumpSpeed;

                jumpSound.Play();
            }
        }

        movement.y -= gravity * Time.deltaTime;
        controller.Move(movement * Time.deltaTime);
    }

    private void OnTriggerEnter (Collider other)
    {
        if (other.tag == "TurnTrigger")
        {
            //transform.Rotate(0, 90, 0);
            //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Vector3.left), 0.2f);
            transform.eulerAngles = other.transform.eulerAngles;
            switch (Orientacion)
            {
                case EstadosOrientacion.Sur:
                    Orientacion = EstadosOrientacion.Este;
                    break;
                case EstadosOrientacion.Este:
                    Orientacion = EstadosOrientacion.Sur;
                    break;
            }
        }
    }
}
