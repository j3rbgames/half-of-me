﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CogerObjeto : MonoBehaviour
{
    public GameObject Cajita;
    //bool cajitaEnRango = false;

    public GameObject Cajon;
    //bool cajonEnRango = false;

    public AudioSource pickBoxSound;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") /*&& cajitaEnRango == true*/ && Vector3.Distance(Cajita.transform.position , transform.position) <= 1.4f)
        {
            print("Fire1 key was pressed (ctrl, mouse1)");
            Cajita.transform.parent = transform;
            pickBoxSound.Play();
        }else{
            Cajita.transform.parent = null;
        }

        if (Input.GetButton("Fire1") /*&& cajonEnRango == true*/ && Vector3.Distance(Cajon.transform.position, transform.position) <= 1.5f)
        {
            print("Fire1 key was pressed (ctrl, mouse1)");
            Cajon.transform.parent = transform;
            pickBoxSound.Play();
        }
        else
        {
            Cajon.transform.parent = null;
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        cajitaEnRango = true;
        cajonEnRango = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        cajitaEnRango = false;
        cajonEnRango = false;
    }*/

}
