﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VidaPlayer : MonoBehaviour
{
    public vida vida_canvas;
    public int vidas = 3;
    public AudioSource hurtSound;
    Vector3 direction = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        vida_canvas = FindObjectOfType<vida>();
    }

    // Update is called once per frame
    void Update()
    {
        vida_canvas.cambioVida(vidas);
        if (direction != Vector3.zero)
        {
            direction.y = 0;
            direction.z = 0;
            this.transform.position += direction * 5f;
            direction = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemigo" || collision.gameObject.tag == "boss")
        {
            vidas--;
            vida_canvas.cambioVida(vidas);
            hurtSound.Play();
            direction = (this.transform.position - collision.transform.position).normalized;
            if (vidas == 0) {
                SceneManager.LoadScene("GameOver");
                //SceneManager.LoadSceneAsync("GameOver", LoadSceneMode.Additive);
                //Time.timeScale = 0f;
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bolaFuego")
        {
            vidas--;
            vida_canvas.cambioVida(vidas);
            hurtSound.Play();
            //direction = (this.transform.position - other.transform.position).normalized;
            if (vidas == 0)
            {
                                SceneManager.LoadScene("GameOver");
                //SceneManager.LoadSceneAsync("GameOver", LoadSceneMode.Additive);
            }
        }
        if (other.gameObject.tag == "recuperarVida" && vidas < 3)
        {
            vidas++;
            vida_canvas.cambioVida(vidas);
        }
    }

}
