﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour
{

    public float MoveSpeed = 10f;
    public float TurnSpeed = 2f;
    public float Gravity = 20f;
    public float Jump = 10f;

    float GravityEffect;

    Vector2 InputAxes;
    float Angle;
    float GroundDistance;

    Quaternion TargetRotation;
    private Rigidbody rb;

    CharacterController Player;

    void Start()
    {
        Player = GetComponent<CharacterController>();
        GroundDistance = Player.bounds.extents.y;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        GetInput();

        if (Mathf.Abs(InputAxes.x) < 1 && Mathf.Abs(InputAxes.y) < 1) return;

        CalculateDirection();
        Rotate();

        ApplyGravity();

        Vector3 Movement = transform.forward * TurnSpeed * Time.deltaTime;
        Movement.y += GravityEffect;
        rb.AddForce(Movement * MoveSpeed);
        Player.Move(Movement);

        PlayerJump();

        //Debug.Log("gravity: " + GravityEffect.ToString());

    }

    void GetInput()
    {
        InputAxes.x = Input.GetAxisRaw("Horizontal");
        InputAxes.y = Input.GetAxisRaw("Vertical");
    }

    void CalculateDirection()
    {
        Angle = Mathf.Atan2(InputAxes.x, InputAxes.y);
        Angle = Mathf.Rad2Deg * Angle;
    }

    void Rotate()
    {
        TargetRotation = Quaternion.Euler(0, Angle, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, TargetRotation, TurnSpeed * Time.deltaTime);
    }

    void PlayerJump()
    {
        if (IsGrounded())
        {
            if (Input.GetButtonDown("Jump"))
            {
                GravityEffect -= Jump * Time.deltaTime;
                Debug.Log("jump");
            }
        }
    }

    void ApplyGravity()
    {
        if (!IsGrounded())
        {
            GravityEffect -= Gravity * Time.deltaTime;
        }
        else
        {
            GravityEffect = 0f;
        }
    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, GroundDistance + 0.1f);
    }

}
