﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class atacar : MonoBehaviour
{
    public GameObject proyectil;
    public AudioSource fireSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire3"))
        {
            GameObject onda = Instantiate(proyectil, transform);
            Rigidbody rb = onda.GetComponent<Rigidbody>();
            rb.velocity = transform.forward * 10;
            rb.transform.parent = null;
            fireSound.Play();
        }
    }
}
