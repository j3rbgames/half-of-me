﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class botonesMP : MonoBehaviour
{
    public VideoPlayer vp;
    public GameObject canvas;

    public Animator jugar;
    public Animator conf;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        checkOver();
        if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            clickArrow();
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            play();
        }
    }

    public void play()
    {
        vp.Play();
        Debug.Log("En play");
        canvas.SetActive(false);
    }

    private void checkOver()
    {

        long playerCurrentFrame = vp.frame;
        long playerFrameCount = Convert.ToInt64(vp.frameCount);

        if (playerCurrentFrame < playerFrameCount-1)
        {
            Debug.Log("Video is playing");
        }
        else
        {
            Debug.Log("Video is finished");
            SceneManager.LoadScene("nivelTutorial");
            //SceneManager.LoadScene("nivelTutorial");
        }
    }

    public void exit()
    {
        Application.Quit();
    }

    public void clickArrow()
    {
        if (jugar.GetBool("dentro"))
        {
            jugar.SetBool("dentro", !jugar.GetBool("dentro"));
            StartCoroutine(confDelay());
            //conf.SetBool("dentro", !conf.GetBool("dentro"));
        }
        else
        {

            conf.SetBool("dentro", !conf.GetBool("dentro"));
            StartCoroutine(playDelay());
            //jugar.SetBool("dentro", !jugar.GetBool("dentro"));
        }

    }


    IEnumerator confDelay()
    {
        yield return new WaitForSeconds(0.5f);
        conf.SetBool("dentro", !conf.GetBool("dentro"));
    }

    IEnumerator playDelay()
    {
        yield return new WaitForSeconds(0.5f);
        jugar.SetBool("dentro", !jugar.GetBool("dentro"));
    }


}
