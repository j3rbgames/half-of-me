﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectil : MonoBehaviour
{
    public GameObject explosionVFX;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 1);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemigo") == true || other.CompareTag("boss") == true)
        {
            Destroy(gameObject);
            Instantiate(explosionVFX, transform.position, transform.rotation);
        }

    }
}
