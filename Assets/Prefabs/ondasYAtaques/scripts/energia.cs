﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energia : MonoBehaviour
{
    public Sprite[] Energia;
    // Start is called before the first frame update
    void Start()
    {
        cambioEnergia(3);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void cambioEnergia(int pos)
    {
        this.GetComponent<Image>().sprite = Energia[pos];
    }
}
