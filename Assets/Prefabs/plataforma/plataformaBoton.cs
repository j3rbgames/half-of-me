﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataformaBoton : MonoBehaviour
{

    public Transform target;
    public float speed;

    private Vector3 startb, endb;

    // Start is called before the first frame update
    void Start()
    {
        target.parent = null;
        startb = transform.position;
        endb = target.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void boton()
    {
        target.parent = null;
        if (target != null)
        {

            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.position, fixedSpeed);
        }
        if (transform.position == target.position)
        {
            target.position = (target.position == startb) ? endb : startb; //lo mismo que poner dos if
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //detectar que el objeto que toca es el person
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent = transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent = null;
        }
    }


}