﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marcador : MonoBehaviour
{
    private Text texto;

    public void Actualizar(int puntos)
    {
        string str = puntos.ToString();
        texto.text = str.PadLeft(3, '0');
    }
    void Awake()
    {
        texto = GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
