﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform jugador;
    public Vector3 desplazamiento = new Vector3(0, (float)2.75, -10);

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector3(desplazamiento.x, jugador.position.y + desplazamiento.y, desplazamiento.z);
    }
}
