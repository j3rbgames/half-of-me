﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controlButtons : MonoBehaviour
{

    public GameObject panelMenu;

    // Start is called before the first frame update
    void Start()
    {

        Time.timeScale = 1f;
        panelMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(panelMenu);

        if (Input.GetButtonDown("Cancel")) 
        {
            Debug.Log("DEntro del cancel");
            Time.timeScale = 0f;
            panelMenu.SetActive(true);
        }
        
    }

    public void restart()
    {
        panelMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void configuration(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void exit(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
