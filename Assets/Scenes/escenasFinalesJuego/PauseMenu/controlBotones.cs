﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controlBotones : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reanudar()
    {
        Debug.Log("Boton reanudar pulsado");
        SceneManager.UnloadSceneAsync("PauseMenu");
        Time.timeScale = 1f;
    }

    public void reiniciar()
    {
        Debug.Log("Boton reiniciar pulsado");
        string scene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(scene);
        Time.timeScale = 1f;
    }

    public void exit()
    {
        Debug.Log("Boton salir pulsado");
        SceneManager.LoadScene("menu");
        Time.timeScale = 1f;
    }

}
