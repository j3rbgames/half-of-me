﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class managerMenuPause : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (Time.timeScale != 0f)
            {
                SceneManager.LoadSceneAsync("PauseMenu", LoadSceneMode.Additive);
                Time.timeScale = 0f;
            }
        }
    }
}
