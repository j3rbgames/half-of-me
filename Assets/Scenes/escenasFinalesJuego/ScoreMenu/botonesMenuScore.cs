﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class botonesMenuScore : MonoBehaviour
{
    
    public GameObject complet;

    public static GameManager manager;

    public Text score;

    // Start is called before the first frame update
    void Start()
    {
        /*
        int scoreN = Random.Range(100, 500);
        score.text = scoreN.ToString();
        */
        complet.GetComponent<Animator>().SetBool("completado", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playNext()
    {
        //Pasar al siguiente nivel
        retry();
    }

    public void retry()
    {
        // volver a cargar el nivel
        Time.timeScale = 1;
        SceneManager.LoadScene("nivelTutorial");
    }

    public void exit()
    {
        //Salir al menu principal
        Time.timeScale = 1;
        SceneManager.LoadScene("menu");
    }
}
